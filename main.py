def print_even_backward(word1, word2):
    words = [
        word1,
        word2,
    ]

    for i in range(5):
        if i % 2: print(' '.join(words[::-1]))
        else:     print(' '.join(words))

if __name__ == '__main__':
    print_even_backward("hello", "world")