import csv
import re


CSV_FILE_NAME = 'user_info.csv'
ERROR = True
OK = False


def name_error_checker(name):
    name = name.split(" ")
    if len(name) >= 3:
        return "You must input `Firstname Lastname`", True

    return "", False


def phone_number_error_checker(phone):
    if bool(re.match('^010-\d{4}-\d{4}$', phone)) is True:
        return "", OK
    if bool(re.match('^010.\d{4}.\d{4}$', phone)) is True:
        return "", OK
    if bool(re.match('^010 \d{4} \d{4}$', phone)) is True:
        return "", OK

    return "Check phone number", ERROR


def email_error_checker(email):
    if bool(re.match('^[a-zA-Z0-9+-_.]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$', email)) == False:
        return "Check email address", ERROR
    
    return "", True


def user_input():
    name = ""; phone = ""; email = ""

    while True: 
        name = input("Please input name (ex. Firstname Lastname) >")
        msg, err = name_error_checker(name)
        if err == ERROR:
            print(msg)
            continue

        phone = input("Please input phone number (ex. 010-0000-0000 or 010.0000.0000 or 010 0000 0000) >")
        msg, err = phone_number_error_checker(phone)
        if err == ERROR:
            print(msg)
            continue

        email = input("Please input email >")
        msg, err = email_error_checker(email)
        if err == ERROR:
            print(msg)
            continue

    return [name, phone, email]


def csv_writer(data):
    with open(CSV_FILE_NAME, 'a', newline='') as csvfile:
        csvwriter = csv.writer(csvfile)
        csvwriter.writerow(data)


def csv_reader(csv_file_name):
    with open(csv_file_name, newline='') as csvfile:
        reader = csv.DictReader(csvfile)
        print()
        print("> " + CSV_FILE_NAME)
        print("=" * 30)
        for row in reader:
            print(row['username'], row['email'], row['phone'])
        print("=" * 30)
        print("Data saved successfully!")
        print()


def main():
    data = user_input()
    csv_writer(data)
    csv_reader(CSV_FILE_NAME)


if __name__ == "__main__":
    main()
