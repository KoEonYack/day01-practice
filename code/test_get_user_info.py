from get_user_info import *


def test_phone_regx():
    assert phone_number_error_checker("010-1234-1234") == ('', False)


def test_email_regx():
    assert email_error_checker("apple@naver.com") == ('', True)


def test_name_checker():
    assert name_error_checker("apple ko") == ('', False)


def main():
    test_phone_regx()
    test_email_regx()
    test_name_checker()


if __name__ == "__main__":
    main()
    print("=====================")
    print("Successfuly run all test case......")
