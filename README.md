# Day01 Practice

## Description

본 프로그램은 사용자의 입력(이름, 이메일, 전화번호)을 CSV파일로 저장하는 코드입니다.

## Run Code

```
python3 get_user_info.py
```

## Run Test Code

```
python3 test_get_user_info.py
```

## Env

python3 >= 3.8

## Liscense

[MIT](./LICENSE)